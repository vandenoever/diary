with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    rustc
    cargo
    clippy
    rustfmt
    qt5.qtquickcontrols
    cmake
    ninja
    openssl
    pkg-config
    valgrind
    gdb
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;
}

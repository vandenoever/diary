#include "highlighter.h"
#include <QtQuick/QQuickTextDocument>
#include <KF5/KSyntaxHighlighting/KSyntaxHighlighting/Definition>
#include <KF5/KSyntaxHighlighting/KSyntaxHighlighting/Repository>
#include <KF5/KSyntaxHighlighting/KSyntaxHighlighting/SyntaxHighlighter>
#include <KF5/KSyntaxHighlighting/KSyntaxHighlighting/Theme>

using namespace KSyntaxHighlighting;

class EmailSyntaxHighlighter::Private {
public:
    Repository repository;
    SyntaxHighlighter highlighter;
    Private() :highlighter((QTextDocument*)0) {
        qDebug() << "Initialize highlighting.";
        const Definition definition = repository.definitionForName("Markdown");
        highlighter.setDefinition(definition);
        highlighter.setTheme(repository.defaultTheme());
    }
};

EmailSyntaxHighlighter::EmailSyntaxHighlighter(QObject *parent)
    : QObject(parent), d(new Private) {
}

EmailSyntaxHighlighter::~EmailSyntaxHighlighter() {
    delete d;
}

void EmailSyntaxHighlighter::setDocument(QQuickTextDocument* document) {
    d->highlighter.setDocument(document->textDocument());
}
void EmailSyntaxHighlighter::setDark(bool dark) {
    d->highlighter.setTheme(d->repository.defaultTheme(dark ? Repository::DefaultTheme::DarkTheme : Repository::Repository::DefaultTheme::LightTheme));
}

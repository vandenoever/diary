use chrono::prelude::Local;
use std::collections::BTreeSet;
use std::fs::File;
use std::io;
use std::io::{BufReader, ErrorKind, Read, Write};
use std::path::PathBuf;
use std::rc::Rc;

#[derive(Clone, Serialize, Deserialize)]
struct TimeRange {
    pub id: String,
}

#[derive(Clone, Serialize, Deserialize)]
struct DiaryData {
    entries: Vec<EntriesItem>,
    time_ranges: Vec<TimeRange>,
}

pub trait ItemTrait {
    fn is_empty(&self) -> bool {
        true
    }
    fn title(&self) -> Option<&str> {
        None
    }
    fn message(&self) -> Option<&str> {
        None
    }
    fn estimated_duration(&self) -> Option<u32> {
        None
    }
    fn created(&self) -> Option<i64> {
        None
    }
    fn deadline(&self) -> Option<i64> {
        None
    }
    fn done(&self) -> Option<bool> {
        None
    }
    fn continued_from(&self) -> Option<&str> {
        None
    }
    fn time_range_group(&self) -> Option<&str> {
        None
    }
    fn set_title(&mut self, _val: Option<&str>) -> bool {
        false
    }
    fn set_message(&mut self, _val: Option<&str>) -> bool {
        false
    }
    fn set_estimated_duration(&mut self, _val: Option<u32>) -> bool {
        false
    }
    fn set_time_range_group(&mut self, _val: Option<&str>) -> bool {
        false
    }
    fn set_created(&mut self, _val: Option<i64>) -> bool {
        false
    }
    fn set_deadline(&mut self, _val: Option<i64>) -> bool {
        false
    }
    fn set_done(&mut self, _val: Option<bool>) -> bool {
        false
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct EntriesItem {
    pub id: String,
    pub current: Item,
    previous: Vec<Item>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Item {
    pub edited: i64,
    pub start: i64,
    pub end: i64,
    pub content: ItemContent,
}

impl Item {
    pub fn new(entry_type: &str) -> Item {
        let content = match entry_type {
            "Article" => ItemContent::Article(Article::default()),
            "Todo" => ItemContent::Todo(Todo::default()),
            entry_type => panic!("Unknown entry type {}", entry_type),
        };
        Item::from_content(content)
    }
    fn from_content(content: ItemContent) -> Item {
        Item {
            edited: seconds_since_epoch(),
            start: minutes_since_epoch_as_secs(),
            end: minutes_since_epoch_as_secs(),
            content,
        }
    }
    pub fn continue_from(&self, id: String) -> Self {
        let content = match &self.content {
            ItemContent::Article(prev) => {
                let a = Article {
                    title: prev.title.clone(),
                    continued_from: Some(id),
                    ..Article::default()
                };
                ItemContent::Article(a)
            }
            ItemContent::Todo(_) => {
                let a = Todo {
                    continued_from: Some(id),
                    ..Todo::default()
                };
                ItemContent::Todo(a)
            }
        };
        Item::from_content(content)
    }
}

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub enum ItemContent {
    Article(Article),
    Todo(Todo),
}

impl ItemContent {
    // comma-separated list of entry types
    pub fn entry_types() -> String {
        "Article,Todo".into()
    }
    pub fn entry_type(&self) -> &str {
        match self {
            ItemContent::Article(_) => "Article",
            ItemContent::Todo(_) => "Todo",
        }
    }
}

fn set_str(s: &mut String, val: Option<&str>) -> bool {
    s.clear();
    if let Some(val) = val {
        s.push_str(val);
    }
    true
}

impl ItemTrait for Item {
    fn is_empty(&self) -> bool {
        match &self.content {
            ItemContent::Article(a) => a.is_empty(),
            ItemContent::Todo(t) => t.is_empty(),
        }
    }
    fn title(&self) -> Option<&str> {
        match &self.content {
            ItemContent::Article(a) => Some(&a.title),
            ItemContent::Todo(_) => None,
        }
    }
    fn set_title(&mut self, val: Option<&str>) -> bool {
        match &mut self.content {
            ItemContent::Article(a) => set_str(&mut a.title, val),
            ItemContent::Todo(_) => false,
        }
    }
    fn message(&self) -> Option<&str> {
        match &self.content {
            ItemContent::Article(a) => Some(&a.message),
            ItemContent::Todo(t) => Some(&t.message),
        }
    }
    fn set_message(&mut self, val: Option<&str>) -> bool {
        match &mut self.content {
            ItemContent::Article(a) => set_str(&mut a.message, val),
            ItemContent::Todo(t) => set_str(&mut t.message, val),
        }
    }
    fn estimated_duration(&self) -> Option<u32> {
        match &self.content {
            ItemContent::Article(_) => None,
            ItemContent::Todo(t) => Some(t.estimated_duration),
        }
    }
    fn set_estimated_duration(&mut self, val: Option<u32>) -> bool {
        match &mut self.content {
            ItemContent::Article(_) => false,
            ItemContent::Todo(t) => {
                if let Some(val) = val {
                    t.estimated_duration = val;
                    true
                } else {
                    false
                }
            }
        }
    }
    fn created(&self) -> Option<i64> {
        match &self.content {
            ItemContent::Article(_) => None,
            ItemContent::Todo(t) => Some(t.created),
        }
    }
    fn set_created(&mut self, val: Option<i64>) -> bool {
        match &mut self.content {
            ItemContent::Article(_) => false,
            ItemContent::Todo(t) => {
                if let Some(val) = val {
                    t.created = val;
                    true
                } else {
                    false
                }
            }
        }
    }
    fn deadline(&self) -> Option<i64> {
        match &self.content {
            ItemContent::Article(_) => None,
            ItemContent::Todo(t) => t.deadline,
        }
    }
    fn set_deadline(&mut self, val: Option<i64>) -> bool {
        match &mut self.content {
            ItemContent::Article(_) => false,
            ItemContent::Todo(t) => {
                t.deadline = val;
                true
            }
        }
    }
    fn done(&self) -> Option<bool> {
        match &self.content {
            ItemContent::Article(_) => None,
            ItemContent::Todo(t) => Some(t.done),
        }
    }
    fn set_done(&mut self, val: Option<bool>) -> bool {
        match &mut self.content {
            ItemContent::Article(_) => false,
            ItemContent::Todo(t) => {
                if let Some(val) = val {
                    t.done = val;
                    true
                } else {
                    false
                }
            }
        }
    }
    fn continued_from(&self) -> Option<&str> {
        match &self.content {
            ItemContent::Article(a) => a.continued_from.as_deref(),
            ItemContent::Todo(t) => t.continued_from.as_deref(),
        }
    }
}

impl ItemTrait for Article {
    fn is_empty(&self) -> bool {
        self.title.is_empty() && self.message.is_empty()
    }
}

impl ItemTrait for Todo {
    fn is_empty(&self) -> bool {
        self.message.is_empty()
    }
}

impl Default for Todo {
    fn default() -> Todo {
        Todo {
            estimated_duration: 3600,
            message: String::new(),
            created: seconds_since_epoch(),
            deadline: None,
            done: false,
            continued_from: None,
        }
    }
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Default)]
pub struct Article {
    pub title: String,
    pub message: String,
    pub continued_from: Option<String>,
}

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub struct Todo {
    pub estimated_duration: u32,
    pub message: String,
    pub created: i64,
    pub deadline: Option<i64>,
    pub done: bool,
    pub continued_from: Option<String>,
}

impl PartialEq for Item {
    fn eq(&self, i: &Item) -> bool {
        self.start == i.start && self.end == i.end && self.content == i.content
    }
}

pub struct DataModel {
    list: Vec<Rc<EntriesItem>>,
    last_edited: Option<String>,
}

impl DataModel {
    pub fn new() -> DataModel {
        DataModel {
            list: Vec::new(),
            last_edited: None,
        }
    }
    pub fn load(&mut self, directory: &Option<String>) {
        if let Some(directory) = &directory {
            let mut path: PathBuf = directory.into();
            path.push("diary.json");
            self.list = load_diary(&path)
                .expect("could not read")
                .into_iter()
                .map(|e| e.into())
                .collect();
            self.sort_entries();
        } else {
            self.list.clear();
        }
    }
    pub fn save(&self, directory: &PathBuf) -> io::Result<()> {
        save_diary(directory, &self.list)
    }
    pub fn get_entry(&self, id: &str) -> Option<&Rc<EntriesItem>> {
        self.list.iter().find(|e| e.id == *id)
    }
    pub fn save_entry(&mut self, id: &str, entry: &Item) {
        let pos = self.list.iter().position(|e| e.id == id);
        if let Some(pos) = pos {
            self.set(pos, entry);
        } else if !entry.is_empty() {
            self.add(id, entry);
        }
    }
    fn set(&mut self, pos: usize, entry: &Item) {
        if self.list[pos].current == *entry {
            return;
        }
        // when we add the entry, the sorting order might change
        let needs_sorting = (pos > 0 && entry.start > self.list[pos - 1].current.start)
            || (pos + 1 < self.list.len() && entry.start < self.list[pos + 1].current.start);
        let mut e = (*self.list[pos]).clone();
        if self.last_edited.as_ref() != Some(&self.list[pos].id) {
            // this entry was the not the last edited, back it up
            let mut entry = entry.clone();
            ::std::mem::swap(&mut entry, &mut e.current);
            e.previous.push(entry);
            self.last_edited = Some(self.list[pos].id.clone());
        } else {
            e.current = entry.clone();
        }
        // detach the items in the other models
        self.list[pos] = Rc::new(e);
        if needs_sorting {
            self.sort_entries();
        }
    }
    fn add(&mut self, id: &str, entry: &Item) {
        self.list.push(Rc::new(EntriesItem {
            id: id.into(),
            current: entry.clone(),
            previous: Default::default(),
        }));
        self.sort_entries();
    }
    pub fn sort_entries(&mut self) {
        self.list
            .sort_unstable_by(|a, b| b.current.start.cmp(&a.current.start));
    }
    pub fn entries(&self) -> &[Rc<EntriesItem>] {
        &self.list
    }
    pub fn set_start_and_end(&mut self, id: &str, start: i64, end: i64) {
        let pos = self.list.iter().position(|e| e.id == id);
        if let Some(pos) = pos {
            let mut new = self.list[pos].current.clone();
            new.start = start;
            new.end = end;
            self.set(pos, &new);
        }
    }
}

fn read_file(path: &PathBuf) -> io::Result<String> {
    let file = File::open(path)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;
    Ok(contents)
}

fn get_ids(data: &DiaryData) -> io::Result<BTreeSet<String>> {
    let mut ids = BTreeSet::new();
    for entry in &data.entries {
        if ids.contains(&entry.id) {
            return Err(io::Error::new(
                ErrorKind::InvalidData,
                format!("Duplicate id: {}", entry.id),
            ));
        }
        ids.insert(entry.id.clone());
    }
    Ok(ids)
}

fn check_continued_from_fields(ids: &BTreeSet<String>, data: &mut DiaryData) -> io::Result<()> {
    for entry in &data.entries {
        if let Some(cf) = entry.current.continued_from() {
            if !ids.contains(cf) {
                return Err(io::Error::new(
                    ErrorKind::InvalidData,
                    format!("Id in continuedFrom {} is not found.", cf),
                ));
            }
        }
    }
    Ok(())
}

fn load_diary(path: &PathBuf) -> io::Result<Vec<EntriesItem>> {
    let contents = read_file(path)?;
    let mut data: DiaryData = serde_json::from_str(&contents)?;
    for e in &mut data.entries {
        if e.current.start > e.current.end {
            e.current.end = e.current.start;
        }
    }
    let ids = get_ids(&data)?;
    check_continued_from_fields(&ids, &mut data)?;
    Ok(data.entries)
}

fn file_equal_to_string(path: &PathBuf, content: &str) -> bool {
    let content_size_equal = if let Ok(metadata) = path.metadata() {
        content.as_bytes().len() == metadata.len() as usize
    } else {
        false
    };
    content_size_equal
        && if let Ok(previous_content) = read_file(path) {
            content == previous_content
        } else {
            false
        }
}

// add any data from old that is not present in entries
fn add_missing_data(old: Vec<EntriesItem>, entries: &mut Vec<EntriesItem>) -> bool {
    let mut data_added = false;
    for entry in old {
        if let Some(item) = entries.iter_mut().find(|e| e.id == entry.id) {
            data_added |= add_missing_item_data(entry, item);
        } else {
            entries.push(entry);
            data_added = true;
        }
    }
    data_added
}

fn add_missing_item_data(mut old: EntriesItem, entries: &mut EntriesItem) -> bool {
    let mut data_added = false;
    if old.current.edited > entries.current.edited {
        // the data from file is newer, make it the current entry
        std::mem::swap(&mut old.current, &mut entries.current);
        // and add entries.current (that moved to old.current) to the
        // list of previous entries if it is not there already
        if !entries
            .previous
            .iter()
            .any(|e| e.edited == old.current.edited)
        {
            entries.previous.push(old.current);
        }
        data_added = true;
    }
    for item in old.previous {
        if !entries.previous.iter().any(|e| e.edited == item.edited) {
            entries.previous.push(item);
            data_added = true;
        }
    }
    entries
        .previous
        .sort_unstable_by(|a, b| b.edited.cmp(&a.edited));
    data_added
}

fn save_diary(directory: &PathBuf, entries: &[Rc<EntriesItem>]) -> io::Result<()> {
    let mut path = directory.clone();
    path.push("diary.json");
    let previous = if let Ok(previous) = load_diary(&path) {
        previous
    } else {
        Vec::new()
    };
    let mut copy: Vec<EntriesItem> = entries.iter().map(|e| (**e).clone()).collect();
    add_missing_data(previous, &mut copy);
    let data = DiaryData {
        entries: copy,
        time_ranges: Vec::new(),
    };
    let json = serde_json::to_string_pretty(&data)?;
    if !file_equal_to_string(&path, &json) {
        let repo = open_git(directory);
        if let Err(e) = &repo {
            println!("{}", e);
        }
        File::create(&path)?.write_all(json.as_bytes())?;
        println!("wrote file");
        if let Ok(repo) = repo {
            if let Err(e) = commit(&repo) {
                println!("{}", e);
            }
        }
    }
    Ok(())
}

pub fn seconds_since_epoch() -> i64 {
    Local::now().timestamp()
}

fn minutes_since_epoch_as_secs() -> i64 {
    let t = seconds_since_epoch();
    t - t % 60
}

use git2::{Commit, ObjectType, Repository, Signature};

fn open_git(directory: &PathBuf) -> Result<Repository, git2::Error> {
    match Repository::open(directory) {
        Ok(repo) => Ok(repo),
        Err(e) => {
            if e.class() == git2::ErrorClass::Repository && e.code() == git2::ErrorCode::NotFound {
                match Repository::init(directory) {
                    Ok(repo) => Ok(repo),
                    Err(e) => Err(e),
                }
            } else {
                Err(e)
            }
        }
    }
}

fn find_last_commit(repo: &Repository) -> Result<Commit, git2::Error> {
    let obj = repo.head()?.resolve()?.peel(ObjectType::Commit)?;
    obj.into_commit()
        .map_err(|_| git2::Error::from_str("Couldn't find commit"))
}

fn commit(repo: &Repository) -> Result<(), git2::Error> {
    let mut index = repo.index()?;
    let path: PathBuf = "diary.json".into();
    index.add_path(&path)?;
    let oid = index.write_tree()?;
    let signature = Signature::now("Jos van den Oever", "jos@vandenoever.info")?;
    let parent_commit;
    let mut parent_commits = Vec::new();
    if let Ok(commit) = find_last_commit(repo) {
        parent_commit = commit;
        parent_commits.push(&parent_commit);
    };
    let tree = repo.find_tree(oid)?;
    repo.commit(
        Some("HEAD"),    // point HEAD to our new commit
        &signature,      // author
        &signature,      // committer
        "save",          // commit message
        &tree,           // tree
        &parent_commits, // parents
    )?;
    // if this is not done, the staged files hang around
    repo.checkout_head(Some(git2::build::CheckoutBuilder::new().force()))?;
    Ok(())
}

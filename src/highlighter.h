#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QtCore/QObject>

class QQuickTextDocument;
class EmailSyntaxHighlighter : public QObject {
    class Private;
    Q_OBJECT
    Q_PROPERTY(QQuickTextDocument* document WRITE setDocument FINAL)
    Q_PROPERTY(bool dark WRITE setDark FINAL)
    Private* const d;
public:
    void setDocument(QQuickTextDocument* document);
    void setDark(bool dark);
    EmailSyntaxHighlighter(QObject *parent = 0);
    ~EmailSyntaxHighlighter();
};

#endif

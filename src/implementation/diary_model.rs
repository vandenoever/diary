use super::super::data_model::*;
use super::*;
use crate::interface::*;
use chrono::{prelude::*, LocalResult};
use std::path::PathBuf;
use uuid::Uuid;

pub struct DiaryModel {
    emit: DiaryModelEmitter,
    entries: Entries,
    todos: Entries,
    day: Entries,
    day_day: i64,
    monday: Entries,
    tuesday: Entries,
    wednesday: Entries,
    thursday: Entries,
    friday: Entries,
    saturday: Entries,
    sunday: Entries,
    entry: Entry,
    directory: Option<PathBuf>,
    data_model: DataModel,
    time_range_groups: TimeRangeGroups,
    search: Search,
}

impl DiaryModel {
    fn save_entry(&mut self) {
        self.entry.entry.edited = seconds_since_epoch();
        self.data_model
            .save_entry(&self.entry.id, &self.entry.entry);
        self.update_models();
    }
    fn save(&mut self) {
        self.save_entry();
        if let Some(directory) = &self.directory {
            self.data_model.save(directory).expect("could not save");
        }
    }
    fn update_day_model(&mut self) {
        let day_day = self.day_day;
        let day_day_end = day_day + 24 * 3600;
        self.day
            .set_entries(self.data_model.entries().iter().filter(|e| {
                let start = e.current.start;
                let end = e.current.end;
                (start >= day_day && start < day_day_end) || (end >= day_day && end < day_day_end)
            }));
    }
    fn today_seconds() -> i64 {
        let date = Local::now();
        let day_secs = date.time().hour() * 3600 + date.time().minute() * 60 + date.time().second();
        date.timestamp() - i64::from(day_secs)
    }
    fn update_weekday_model(entries: &mut Entries, n: i64, day_day: i64, data_model: &DataModel) {
        if let LocalResult::Single(date) = Local.timestamp_opt(day_day, 0) {
            let day_secs =
                date.time().hour() * 3600 + date.time().minute() * 60 + date.time().second();
            let number = i64::from(date.weekday().num_days_from_monday()) - n;
            let day_start = day_day - number * 24 * 3600 - i64::from(day_secs);
            let day_end = day_start + 24 * 3600;
            entries.set_entries(data_model.entries().iter().filter(|e| {
                let start = e.current.start;
                let end = e.current.end;
                (start >= day_start && start < day_end) || (end >= day_start && end < day_end)
            }));
        }
    }
    fn update_models(&mut self) {
        self.entries.set_entries(self.data_model.entries().iter());
        self.todos
            .set_entries(self.data_model.entries().iter().filter(|entry| {
                if let ItemContent::Todo(todo) = &entry.current.content {
                    !todo.done
                } else {
                    false
                }
            }));
        self.search.set_entries(self.data_model.entries().iter());
        self.update_day_model();
        DiaryModel::update_weekday_model(&mut self.monday, 0, self.day_day, &self.data_model);
        DiaryModel::update_weekday_model(&mut self.tuesday, 1, self.day_day, &self.data_model);
        DiaryModel::update_weekday_model(&mut self.wednesday, 2, self.day_day, &self.data_model);
        DiaryModel::update_weekday_model(&mut self.thursday, 3, self.day_day, &self.data_model);
        DiaryModel::update_weekday_model(&mut self.friday, 4, self.day_day, &self.data_model);
        DiaryModel::update_weekday_model(&mut self.saturday, 5, self.day_day, &self.data_model);
        DiaryModel::update_weekday_model(&mut self.sunday, 6, self.day_day, &self.data_model);
    }
}

impl Drop for DiaryModel {
    fn drop(&mut self) {
        self.save();
    }
}

impl DiaryModelTrait for DiaryModel {
    fn new(
        emit: DiaryModelEmitter,
        day: Entries,
        entries: Entries,
        entry: Entry,
        friday: Entries,
        monday: Entries,
        saturday: Entries,
        search: Search,
        sunday: Entries,
        thursday: Entries,
        time_range_groups: TimeRangeGroups,
        todos: Entries,
        tuesday: Entries,
        wednesday: Entries,
    ) -> DiaryModel {
        let data_model = DataModel::new();
        DiaryModel {
            data_model,
            day,
            day_day: DiaryModel::today_seconds(),
            directory: None,
            emit,
            entries,
            todos,
            entry,
            friday,
            monday,
            saturday,
            sunday,
            thursday,
            time_range_groups,
            tuesday,
            wednesday,
            search,
        }
    }
    fn emit(&mut self) -> &mut DiaryModelEmitter {
        &mut self.emit
    }
    fn directory(&self) -> Option<&str> {
        self.directory.as_ref().and_then(|d| d.to_str())
    }
    fn set_directory(&mut self, directory: Option<String>) {
        self.save();
        self.data_model.load(&directory);
        self.directory = directory.map(|d| d.into());
        self.emit.directory_changed();
        self.update_models();
    }
    fn entries(&self) -> &Entries {
        &self.entries
    }
    fn entries_mut(&mut self) -> &mut Entries {
        &mut self.entries
    }
    fn todos(&self) -> &Entries {
        &self.todos
    }
    fn todos_mut(&mut self) -> &mut Entries {
        &mut self.todos
    }
    fn day(&self) -> &Entries {
        &self.day
    }
    fn day_mut(&mut self) -> &mut Entries {
        &mut self.day
    }
    fn day_day(&self) -> i64 {
        self.day_day
    }
    fn set_day_day(&mut self, v: i64) {
        if self.day_day != v {
            self.day_day = v;
            self.emit.day_day_changed();
            self.update_models();
        }
    }
    fn monday(&self) -> &Entries {
        &self.monday
    }
    fn monday_mut(&mut self) -> &mut Entries {
        &mut self.monday
    }
    fn tuesday(&self) -> &Entries {
        &self.tuesday
    }
    fn tuesday_mut(&mut self) -> &mut Entries {
        &mut self.tuesday
    }
    fn wednesday(&self) -> &Entries {
        &self.wednesday
    }
    fn wednesday_mut(&mut self) -> &mut Entries {
        &mut self.wednesday
    }
    fn thursday(&self) -> &Entries {
        &self.thursday
    }
    fn thursday_mut(&mut self) -> &mut Entries {
        &mut self.thursday
    }
    fn friday(&self) -> &Entries {
        &self.friday
    }
    fn friday_mut(&mut self) -> &mut Entries {
        &mut self.friday
    }
    fn saturday(&self) -> &Entries {
        &self.saturday
    }
    fn saturday_mut(&mut self) -> &mut Entries {
        &mut self.saturday
    }
    fn sunday(&self) -> &Entries {
        &self.sunday
    }
    fn sunday_mut(&mut self) -> &mut Entries {
        &mut self.sunday
    }
    fn entry(&self) -> &Entry {
        &self.entry
    }
    fn entry_mut(&mut self) -> &mut Entry {
        &mut self.entry
    }
    fn new_entry(&mut self, entry_type: String) {
        self.save_entry();
        self.entry
            .set(Uuid::new_v4().to_string(), &Item::new(&entry_type));
    }
    fn continue_from(&mut self, id: String) {
        self.save_entry();
        let entry = self.data_model.get_entry(&id);
        if let Some(entry) = entry {
            let item = entry.current.continue_from(id);
            self.entry.set(Uuid::new_v4().to_string(), &item);
        }
    }
    fn set_edited_entry(&mut self, id: String) {
        self.save_entry();
        if let Some(entry) = self.data_model.get_entry(&id) {
            self.entry.set(id, &entry.current);
        }
    }
    fn set_start_and_end(&mut self, id: String, start: i64, end: i64) {
        self.data_model.set_start_and_end(&id, start, end);
        self.update_models();
    }
    fn entry_types(&self) -> String {
        ItemContent::entry_types()
    }
    fn time_range_groups(&self) -> &TimeRangeGroups {
        &self.time_range_groups
    }
    fn time_range_groups_mut(&mut self) -> &mut TimeRangeGroups {
        &mut self.time_range_groups
    }
    fn search(&self) -> &Search {
        &self.search
    }
    fn search_mut(&mut self) -> &mut Search {
        &mut self.search
    }
    fn save(&mut self) {
        self.save();
    }
}

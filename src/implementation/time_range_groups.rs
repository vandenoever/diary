use crate::interface::*;

struct TimeRangeGroupsItem {
    id: String,
    title: String,
    description: String,
}

pub struct TimeRangeGroups {
    emit: TimeRangeGroupsEmitter,
    list: Vec<TimeRangeGroupsItem>,
}

impl TimeRangeGroupsTrait for TimeRangeGroups {
    fn new(emit: TimeRangeGroupsEmitter, _model: TimeRangeGroupsList) -> TimeRangeGroups {
        TimeRangeGroups {
            emit,
            list: Vec::new(),
        }
    }
    fn emit(&mut self) -> &mut TimeRangeGroupsEmitter {
        &mut self.emit
    }
    fn row_count(&self) -> usize {
        self.list.len()
    }
    fn id(&self, index: usize) -> &str {
        &self.list[index].id
    }
    fn title(&self, index: usize) -> &str {
        &self.list[index].title
    }
    fn description(&self, index: usize) -> &str {
        &self.list[index].description
    }
}

use super::super::data_model::*;
use crate::interface::*;
use uuid::Uuid;

pub struct Entry {
    emit: EntryEmitter,
    rendered: Option<String>,
    pub id: String,
    pub entry: Item,
}

impl Entry {
    pub fn set(&mut self, id: String, entry: &Item) {
        let old_entry = self.entry.clone();
        self.entry = entry.clone();
        if self.id != id {
            self.id = id;
            self.emit.id_changed();
        }
        if old_entry.content.entry_type() != entry.content.entry_type() {
            self.emit.entry_type_changed();
        }
        if old_entry.start != entry.start {
            self.emit.start_changed();
        }
        if old_entry.end != entry.end {
            self.emit.end_changed();
        }
        if old_entry.title() != entry.title() {
            self.emit.title_changed();
        }
        if old_entry.message() != entry.message() {
            self.render();
            self.emit.message_changed();
        }
        if old_entry.estimated_duration() != entry.estimated_duration() {
            self.emit.estimated_duration_changed();
        }
        if old_entry.done() != entry.done() {
            self.emit.done_changed();
        }
        if old_entry.created() != entry.created() {
            self.emit.created_changed();
        }
    }
    fn render(&mut self) {
        let new_rendered = if let Some(msg) = self.message() {
            /*
            use pulldown_cmark::{html, Parser};
            let parser = Parser::new(msg);
            let mut out = String::new();
            html::push_html(&mut out, parser);
            */
            use comrak::*;
            let out = markdown_to_html(msg, &ComrakOptions::default());
            Some(out)
        } else {
            None
        };
        if new_rendered != self.rendered {
            self.rendered = new_rendered;
            self.emit.rendered_changed();
        }
    }
}

impl EntryTrait for Entry {
    fn new(emit: EntryEmitter) -> Entry {
        Entry {
            emit,
            rendered: None,
            id: Uuid::new_v4().to_string(),
            entry: Item::new("Article"),
        }
    }
    fn emit(&mut self) -> &mut EntryEmitter {
        &mut self.emit
    }
    fn id(&self) -> &str {
        &self.id
    }
    fn entry_type(&self) -> &str {
        self.entry.content.entry_type()
    }
    fn start(&self) -> i64 {
        self.entry.start
    }
    fn set_start(&mut self, v: i64) {
        if self.entry.start != v {
            self.entry.start = v;
            self.emit.start_changed();
        }
    }
    fn end(&self) -> i64 {
        self.entry.end
    }
    fn set_end(&mut self, v: i64) {
        if self.entry.end != v {
            self.entry.end = v;
            self.emit.end_changed();
        }
    }
    fn title(&self) -> Option<&str> {
        self.entry.title()
    }
    fn message(&self) -> Option<&str> {
        self.entry.message()
    }
    fn set_title(&mut self, title: Option<String>) {
        let title = title.as_deref();
        if self.entry.title() != title && self.entry.set_title(title) {
            self.emit.title_changed();
        }
    }
    fn set_message(&mut self, message: Option<String>) {
        let message = message.as_deref();
        if self.entry.message() != message && self.entry.set_message(message) {
            self.render();
            self.emit.message_changed();
        }
    }
    fn estimated_duration(&self) -> Option<u32> {
        self.entry.estimated_duration()
    }
    fn set_estimated_duration(&mut self, v: Option<u32>) {
        if self.entry.estimated_duration() != v && self.entry.set_estimated_duration(v) {
            self.emit.estimated_duration_changed();
        }
    }
    fn done(&self) -> Option<bool> {
        self.entry.done()
    }
    fn set_done(&mut self, done: Option<bool>) {
        if self.entry.done() != done && self.entry.set_done(done) {
            self.emit.done_changed();
        }
    }
    fn continued_from(&self) -> Option<&str> {
        self.entry.continued_from()
    }
    fn created(&self) -> Option<i64> {
        self.entry.created()
    }
    fn set_created(&mut self, created: Option<i64>) {
        if self.entry.created() != created && self.entry.set_created(created) {
            self.emit.created_changed();
        }
    }
    fn deadline(&self) -> Option<i64> {
        self.entry.deadline()
    }
    fn set_deadline(&mut self, deadline: Option<i64>) {
        if self.entry.deadline() != deadline && self.entry.set_deadline(deadline) {
            self.emit.deadline_changed();
        }
    }
    fn time_range_group(&self) -> Option<&str> {
        self.entry.time_range_group()
    }
    fn set_time_range_group(&mut self, time_range_group: Option<String>) {
        let time_range_group = time_range_group.as_deref();
        if self.entry.time_range_group() != time_range_group
            && self.entry.set_time_range_group(time_range_group)
        {
            self.emit.time_range_group_changed();
        }
    }
    fn render_error(&self) -> Option<&str> {
        None
    }
    fn rendered(&self) -> Option<&str> {
        self.rendered.as_deref()
    }
}

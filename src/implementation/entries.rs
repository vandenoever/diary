use super::super::data_model::*;
use crate::interface::*;
use chrono::prelude::*;
use std::iter;
use std::rc::Rc;

pub struct Entries {
    emit: EntriesEmitter,
    model: EntriesList,
    list: Vec<Rc<EntriesItem>>,
    cols: Vec<(u32, u32)>,
}

impl Entries {
    pub fn set_entries<'a, I>(&mut self, entries: I)
    where
        I: Iterator<Item = &'a Rc<EntriesItem>>,
    {
        self.model.begin_reset_model();
        self.list.clear();
        self.list.extend(entries.cloned());
        self.update_cols();
        self.model.end_reset_model();
    }
    /// Place articles that overlap in time into columns.
    /// This is required to lay out items in day or week view.
    fn update_cols(&mut self) {
        self.cols.clear();
        if self.list.is_empty() {
            return;
        }
        // place default values where each article is the only article in
        // its column
        self.cols.extend(iter::repeat((0, 1)).take(self.list.len()));
        let mut i = self.update_col_group(0);
        while i < self.list.len() {
            i = self.update_col_group(i);
        }
    }
    fn update_col_group(&mut self, first_pos: usize) -> usize {
        let len = self.list.len();
        let l = &self.list;
        let mut min_start = l[first_pos].current.start;
        let mut pos = first_pos + 1;
        while pos < len && (l[pos].current.start + 3600).max(l[pos].current.end) > min_start {
            assert!(l[pos - 1].current.start >= l[pos].current.start);
            min_start = min_start.min(l[pos].current.start);
            pos += 1;
        }
        for i in first_pos..pos {
            let n = pos - first_pos;
            let pos = n + first_pos - 1 - i;
            self.cols[i] = (pos as u32, n as u32);
        }
        pos
    }
}

impl EntriesTrait for Entries {
    fn new(emit: EntriesEmitter, model: EntriesList) -> Entries {
        Entries {
            emit,
            model,
            list: Vec::new(),
            cols: Vec::new(),
        }
    }
    fn emit(&mut self) -> &mut EntriesEmitter {
        &mut self.emit
    }
    fn row_count(&self) -> usize {
        self.list.len()
    }
    fn id(&self, index: usize) -> &str {
        &self.list[index].id
    }
    fn start(&self, index: usize) -> i64 {
        self.list[index].current.start
    }
    fn start_date(&self, index: usize) -> String {
        let secs_since_epoch = self.list[index].current.start;
        if let Some(date) = NaiveDateTime::from_timestamp_opt(secs_since_epoch, 0) {
            format!("{}", date.date())
        } else {
            String::new()
        }
    }
    fn end(&self, index: usize) -> i64 {
        self.list[index].current.end
    }
    fn title(&self, index: usize) -> Option<&str> {
        self.list[index].current.title()
    }
    fn message(&self, index: usize) -> Option<&str> {
        self.list[index].current.message()
    }
    fn estimated_duration(&self, index: usize) -> Option<u32> {
        self.list[index].current.estimated_duration()
    }
    fn created(&self, index: usize) -> Option<i64> {
        self.list[index].current.created()
    }
    fn deadline(&self, index: usize) -> Option<i64> {
        self.list[index].current.deadline()
    }
    fn done(&self, index: usize) -> Option<bool> {
        self.list[index].current.done()
    }
    fn continued_from(&self, index: usize) -> Option<&str> {
        self.list[index].current.continued_from()
    }
    fn time_range_group(&self, index: usize) -> Option<&str> {
        self.list[index].current.time_range_group()
    }
    fn render_column(&self, index: usize) -> u32 {
        self.cols[index].0
    }
    fn render_columns(&self, index: usize) -> u32 {
        self.cols[index].1
    }
    fn get_entry_index(&self, id: String) -> i32 {
        if let Some(index) = self.list.iter().position(|item| item.id == id) {
            index as i32
        } else {
            -1
        }
    }
}

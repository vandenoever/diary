use super::*;
use crate::data_model::{EntriesItem, ItemTrait};
use crate::interface::*;
use regex::{Regex, RegexBuilder};
use std::rc::Rc;

pub struct Search {
    emit: SearchEmitter,
    hits: Entries,
    all_entries: Vec<Rc<EntriesItem>>,
    query: String,
}

fn string_to_regex(str: &str) -> Regex {
    if let Ok(re) = RegexBuilder::new(str).case_insensitive(true).build() {
        re
    } else {
        Regex::new(&::regex::escape(str)).unwrap()
    }
}

fn matches(re: &[Regex], txt: &Option<&str>) -> bool {
    if let Some(txt) = txt {
        re.iter().all(|re| re.is_match(txt))
    } else {
        false
    }
}

impl Search {
    pub fn set_entries<'a, I>(&mut self, entries: I)
    where
        I: Iterator<Item = &'a Rc<EntriesItem>>,
    {
        self.all_entries.clear();
        self.all_entries.extend(entries.cloned());
        self.filter();
    }
    fn filter(&mut self) {
        let re: Vec<_> = self.query.split(' ').map(string_to_regex).collect();
        self.hits
            .set_entries(self.all_entries.iter().filter(|entry| {
                matches(&re, &entry.current.title()) || matches(&re, &entry.current.message())
            }));
        self.emit.hits_count_changed();
    }
}

impl SearchTrait for Search {
    fn new(emit: SearchEmitter, hits: Entries) -> Self {
        Search {
            emit,
            hits,
            query: String::new(),
            all_entries: Vec::new(),
        }
    }
    fn emit(&mut self) -> &mut SearchEmitter {
        &mut self.emit
    }
    fn hits(&self) -> &Entries {
        &self.hits
    }
    fn hits_mut(&mut self) -> &mut Entries {
        &mut self.hits
    }
    fn hits_count(&self) -> u64 {
        self.hits.row_count() as u64
    }
    fn query(&self) -> &str {
        &self.query
    }
    fn set_query(&mut self, value: String) {
        self.query = value;
        self.filter();
    }
}

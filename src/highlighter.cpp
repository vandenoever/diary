#include "highlighter.h"
#include <QtCore/QRegularExpression>
#include <QtCore/QDebug>
#include <QtGui/QSyntaxHighlighter>
#include <QtGui/QTextDocument>
#include <QtQuick/QQuickTextDocument>

class QQuickTextDocument;
class MailSyntaxHighlighter : public QSyntaxHighlighter {
public:
    MailSyntaxHighlighter();
protected:
    void highlightBlock(const QString &text);
private:
    const QRegularExpression pattern;
    const QTextCharFormat format;
};

namespace {
    QTextCharFormat greenFormat() {
        QTextCharFormat format;
        return format;
    }
}

class EmailSyntaxHighlighter::Private {
public:
    MailSyntaxHighlighter highlighter;
};

MailSyntaxHighlighter::MailSyntaxHighlighter() :QSyntaxHighlighter((QTextDocument*)0), pattern("^$"), format(greenFormat()) {
    qDebug() << "No highlighting enabled.";
}

void
MailSyntaxHighlighter::highlightBlock(const QString &text) {
    QRegularExpressionMatchIterator matchIterator = pattern.globalMatch(text);
    while (matchIterator.hasNext()) {
        QRegularExpressionMatch match = matchIterator.next();
        setFormat(match.capturedStart(), 1, format);
    }
}

EmailSyntaxHighlighter::EmailSyntaxHighlighter(QObject *parent)
    : QObject(parent), d(new Private) {
}

EmailSyntaxHighlighter::~EmailSyntaxHighlighter() {
    delete d;
}

void EmailSyntaxHighlighter::setDocument(QQuickTextDocument* document) {
    d->highlighter.setDocument(document->textDocument());
}

void EmailSyntaxHighlighter::setDark(bool dark) {
    d->highlighter.setDark(dark);
}

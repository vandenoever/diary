#include "Bindings.h"
#include "highlighter.h"
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtGui/QFontDatabase>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQml/qqml.h>
#include <QtWidgets/QApplication>

extern "C" {
    int main_cpp(const char* app, const char* rawConfigFile);
}

int main_cpp(const char* appPath, const char* diaryPath)
{
    Q_INIT_RESOURCE(qml);
    int argc = 1;
    char* argv[1] = { (char*)appPath };
    // A QGuiApplication would be nicer, but then the desktop style is not used
    QApplication app(argc, argv);
    qmlRegisterType<DiaryModel>("RustCode", 1, 0, "DiaryModel");
    qmlRegisterType<EmailSyntaxHighlighter>("RustCode", 1, 0, "EmailSyntaxHighlighter");
    QString directory = QString::fromUtf8(diaryPath);
    QFileInfo dir(directory);
    if (!dir.isDir()) {
        qCritical() << "Usage: " << argv[0] << " DIRECTORY";
        exit(1);
    }

    QQmlApplicationEngine engine;
    QQmlContext* context = engine.rootContext();
    context->setContextProperty("diaryDirectory", directory);

    const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    context->setContextProperty("fixedFont", fixedFont);

    if (QFile("qml/main.qml").exists()) {
        engine.load(QUrl(QStringLiteral("qml/main.qml")));
    } else {
        engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    }
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

#[macro_use]
extern crate serde_derive;

mod data_model;
mod implementation;
pub mod interface {
    include!(concat!(env!("OUT_DIR"), "/src/interface.rs"));
}

use std::{os::raw::c_char, path::PathBuf};
extern "C" {
    fn main_cpp(app: *const c_char, config_file: *const c_char);
}

fn main() {
    use std::ffi::CString;
    let mut args = ::std::env::args();
    let app = CString::new(args.next().unwrap()).unwrap();
    let diary_dir = args
        .next()
        .or_else(|| {
            std::env::var("HOME")
                .map(|d| format!("{}", PathBuf::from(d).join("diary").display()))
                .ok()
        })
        .unwrap();
    let diary_dir = CString::new(diary_dir).unwrap();
    unsafe {
        main_cpp(app.as_ptr(), diary_dir.as_ptr());
    }
}

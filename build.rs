use rust_qt_binding_generator::build::QtModule;
use std::env::var;
use std::path::PathBuf;

fn find_path(env_var: &str, suffix: &str) -> Option<PathBuf> {
    let value = match var(env_var) {
        Ok(value) => value,
        Err(_) => return None,
    };
    let paths = value.split(':');
    for path in paths {
        let path = PathBuf::from(path);
        let sub_path = path.join(suffix);
        if sub_path.exists() {
            return Some(path);
        }
    }
    None
}

fn cmake_include_path(relative_header_path: &str) -> Option<PathBuf> {
    find_path("CMAKE_INCLUDE_PATH", relative_header_path)
}

fn cmake_library_path(package: &str) -> Option<PathBuf> {
    let lib = format!("lib{}.so", package);
    find_path("CMAKE_LIBRARY_PATH", &lib).map(|lib_dir| lib_dir.join(lib))
}

fn find_kf5_syntax_highlighting() -> Option<(PathBuf, PathBuf)> {
    let package = "KF5SyntaxHighlighting";
    let include =
        cmake_include_path("KF5/KSyntaxHighlighting/KSyntaxHighlighting/SyntaxHighlighter");
    let library = cmake_library_path(package);
    println!("{:?}", include);
    println!("{:?}", library);
    if let (Some(include), Some(lib)) = (include, library) {
        Some((include, lib))
    } else {
        None
    }
}

fn main() {
    let out_dir = ::std::env::var("OUT_DIR").unwrap();
    let mut build = rust_qt_binding_generator::build::Build::new(out_dir);
    build.bindings("bindings.json").qrc("qml/qml.qrc");
    if let Some((include, lib)) = find_kf5_syntax_highlighting() {
        build.h("src/highlighter.h");
        build.cpp("src/khighlighter.cpp");
        build.include_path(include);
        build.link_lib(lib);
    } else {
        build.h("src/highlighter.h");
        build.cpp("src/highlighter.cpp");
    }
    build
        .cpp("src/main.cpp")
        .module(QtModule::Gui)
        .module(QtModule::Qml)
        .module(QtModule::Quick)
        .module(QtModule::Widgets)
        .module(QtModule::Kirigami2)
        .compile("diary");
}

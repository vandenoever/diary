A diary to write things in.

bindings.json defines the interface between the Qt and Rust code.

Build this code with

```bash
mkdir build
cd build
cmake ..
make
```

There is also a Nix flake. Diary can be run as a flake with

```bash
nix flake run . ~/diary
```

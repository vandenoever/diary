import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import RustCode 1.0

ColumnLayout {
    id: tabView

    function setCurrentEntry(id) {
        list.setCurrentEntry(id);
        bar.currentIndex = 0;
    }

    width: parent.width

    DiaryModel {
        id: diaryModel

        directory: diaryDirectory
    }

    Timer {
        interval: 300000 // 5 minutes
        onTriggered: diaryModel.save()
        repeat: true
    }

    TabBar {
        id: bar

        width: parent.width

        TabButton {
            text: qsTr("list")
            width: implicitWidth
        }

        TabButton {
            text: qsTr("todo")
            width: implicitWidth
        }

        TabButton {
            text: qsTr("day")
            width: implicitWidth
        }

        TabButton {
            text: qsTr("search")
            width: implicitWidth
        }

    }

    StackLayout {
        width: parent.width
        currentIndex: bar.currentIndex

        DiaryWithSideBar {
            id: list

            model: diaryModel.entries
        }

        Day {
            id: day

            onNewDay: diaryModel.dayDay = day
            onEntryActivated: setCurrentEntry(id)
        }

        Week {
            id: week

            onNewDay: diaryModel.dayDay = day
            onEntryActivated: setCurrentEntry(id)
        }

        Search {
            onEntryActivated: setCurrentEntry(id)
        }

    }

}

import "DateFunctions.js" as DateFunctions
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

RowLayout {
    property int day
    property int step: 1

    signal newDay(int day)

    function currentDay() {
        const date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date.getTime() / 1000;
    }

    Button {
        text: "<"
        onClicked: newDay(day - step * 24 * 3600)
    }

    Button {
        text: qsTr("Today")
        onClicked: newDay(currentDay())
    }

    Button {
        text: ">"
        onClicked: newDay(day + step * 24 * 3600)
    }

    Label {
        text: DateFunctions.addWeekDayToDate(locale, new Date(day * 1000))
    }

}

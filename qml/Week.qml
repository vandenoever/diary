import "DateFunctions.js" as DateFunctions
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ColumnLayout {
    id: columnLayout

    signal newDay(int day)
    signal entryActivated(string id)

    function getWeekStartSeconds(daySeconds) {
        const dayDate = new Date(daySeconds * 1000);
        const daysFromMonday = (dayDate.getDay() + 6) % 7;
        return dayDate.getTime() / 1000 - daysFromMonday * 24 * 3600;
    }

    QtObject {
        id: d

        property int pixelsPerHour: 60
        property int height: pixelsPerHour * 24
        property real columnWidth: (columnLayout.width - labelMetrics.width) / 7
        property int weekStartSeconds: getWeekStartSeconds(daySelector.day)
    }

    DaySelector {
        id: daySelector

        day: diaryModel.dayDay
        step: 7
        onNewDay: columnLayout.newDay(day)
    }

    TextMetrics {
        id: labelMetrics

        text: " 00:00 "
    }

    Item {
        Layout.fillWidth: true
        height: labelMetrics.height

        Repeater {
            model: 7

            Label {
                id: label

                text: DateFunctions.fullWeekDay(locale, index)
                x: labelMetrics.width + index * d.columnWidth
                width: d.columnWidth
                clip: true
            }

        }

    }

    Flickable {
        id: flickable

        clip: true
        Layout.fillWidth: true
        Layout.fillHeight: true
        contentWidth: width
        contentHeight: d.height
        contentY: contentHeight - height

        Repeater {
            model: 24

            delegate: Item {
                y: index * d.pixelsPerHour

                Label {
                    width: labelMetrics.width
                    text: ((index > 9) ? "" : "0") + index + ":00"
                }

                Rectangle {
                    width: flickable.width - labelMetrics.width
                    height: d.pixelsPerHour
                    x: labelMetrics.width
                    color: (index % 2) ? "transparent" : palette.base
                }

            }

        }

        DayColumn {
            x: labelMetrics.width
            width: d.columnWidth
            height: flickable.contentHeight
            model: diaryModel.monday
            columnStart: d.weekStartSeconds
            onEntryActivated: columnLayout.entryActivated(id)
        }

        DayColumn {
            x: labelMetrics.width + d.columnWidth
            width: d.columnWidth
            height: flickable.contentHeight
            model: diaryModel.tuesday
            columnStart: d.weekStartSeconds + 24 * 3600
            onEntryActivated: columnLayout.entryActivated(id)
        }

        DayColumn {
            x: labelMetrics.width + 2 * d.columnWidth
            width: d.columnWidth
            height: flickable.contentHeight
            model: diaryModel.wednesday
            columnStart: d.weekStartSeconds + 2 * 24 * 3600
            onEntryActivated: columnLayout.entryActivated(id)
        }

        DayColumn {
            x: labelMetrics.width + 3 * d.columnWidth
            width: d.columnWidth
            height: flickable.contentHeight
            model: diaryModel.thursday
            columnStart: d.weekStartSeconds + 3 * 24 * 3600
            onEntryActivated: columnLayout.entryActivated(id)
        }

        DayColumn {
            x: labelMetrics.width + 4 * d.columnWidth
            width: d.columnWidth
            height: flickable.contentHeight
            model: diaryModel.friday
            columnStart: d.weekStartSeconds + 4 * 24 * 3600
            onEntryActivated: columnLayout.entryActivated(id)
        }

        DayColumn {
            x: labelMetrics.width + 5 * d.columnWidth
            width: d.columnWidth
            height: flickable.contentHeight
            model: diaryModel.saturday
            columnStart: d.weekStartSeconds + 5 * 24 * 3600
            onEntryActivated: columnLayout.entryActivated(id)
        }

        DayColumn {
            x: labelMetrics.width + 6 * d.columnWidth
            width: d.columnWidth
            height: flickable.contentHeight
            model: diaryModel.sunday
            columnStart: d.weekStartSeconds + 6 * 24 * 3600
            onEntryActivated: columnLayout.entryActivated(id)
        }

    }

}

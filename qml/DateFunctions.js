function secToDate(sec) {
    return new Date(sec * 1000);
}

function formatTimeRange(locale, start, end) {
    const s = secToDate(start);
    const str = s.toLocaleString(locale, "hh:mm");
    if (start === end) {
        return str;
    }
    const e = secToDate(end);
    const sameDay = s.toDateString() === e.toDateString();
    if (!sameDay) {
        return str + "–...";
    }
    return str + e.toLocaleString(locale, "–hh:mm");
}

function formatSecsRange(locale, start, end) {
    return formatDateRange(locale, secToDate(start), secToDate(end));
}

function formatDateRange(locale, start, end) {
    const str = start.toLocaleString(locale, "ddd yyyy-MM-dd hh:mm");
    if (start.valueOf() === end.valueOf()) {
        return str;
    }
    const sameDay = start.toDateString() === end.toDateString();
    if (sameDay) {
        return str + end.toLocaleString(locale, "–hh:mm");
    } else {
        return str + end.toLocaleString(locale, "–ddd yyyy-MM-dd hh:mm");
    }
}

function formatDate(locale, date) {
    return date.toLocaleString(locale, "yyyy-MM-dd hh:mm");
}

function formatHour(locale, date) {
    return date.toLocaleString(locale, "hh:mm");
}

function formatSecsHour(locale, seconds) {
    return formatHour(locale, secToDate(seconds));
}

function addWeekDayToDate(locale, date) {
    return new Date(date).toLocaleString(locale, "ddd yyyy-MM-dd");
}

function fullWeekDay(locale, dayNumber) {
    const date = new Date((dayNumber + 4) * 24 * 3600 * 1000);
    return date.toLocaleString(locale, "dddd");
}

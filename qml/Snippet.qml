import "DateFunctions.js" as DateFunctions
import QtQuick 2.15
import QtQuick.Layouts 1.15

ColumnLayout {
    property var itemModel
    property color color
    property bool showTime: true

    function checkBox(done) {
        if (done === true)
            return "☑";
        else if (done === false)
            return "☐";
    }

    Text {
        id: time

        color: parent.color
        enabled: showTime
        visible: showTime
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        horizontalAlignment: Text.AlignRight
        text: DateFunctions.formatTimeRange(locale, itemModel.start, itemModel.end)
    }

    Text {
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        color: parent.color
        font.bold: true
        text: checkBox(itemModel.done) || itemModel.title || ""
    }

    Text {
        id: main

        color: parent.color
        Layout.fillWidth: true
        Layout.fillHeight: true
        wrapMode: Text.Wrap
        text: itemModel.message
        maximumLineCount: 3
        elide: Text.ElideRight
    }

}

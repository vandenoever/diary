import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 1280
    height: 1000
    title: qsTr("Diary")
    locale: Qt.locale("nl")

    MainForm {
        anchors.fill: parent
    }

}

import "DateFunctions.js" as DateFunctions
import QtQuick 2.15
import QtQuick.Layouts 1.15

Item {
    id: item

    property var model

    signal entryActivated(int index, string id)

    function setCurrentEntry(id) {
        list.currentIndex = model.getEntryIndex(id);
    }

    ListView {
        id: list

        anchors.fill: parent
        model: parent.model
        delegate: delegate
        section.property: "startDate"
        section.delegate: sectionHeading
        highlightMoveDuration: 200
        highlightMoveVelocity: -1
        clip: true
    }

    Component {
        id: sectionHeading

        Rectangle {
            width: parent.width
            height: childrenRect.height
            color: palette.button

            Text {
                text: DateFunctions.addWeekDayToDate(locale, section)
                color: palette.text
                font.bold: true
                font.pixelSize: 20
            }

            MouseArea {
                id: headerMouseArea

                anchors.fill: parent
                onClicked: {
                    console.log("clicked the header " + section);
                }
            }

        }

    }

    Component {
        id: delegate

        Rectangle {
            id: rectangle

            width: item.width
            height: text.height
            color: palette.window
            states: [
                State {
                    when: list.currentIndex == index || mouseArea.containsMouse

                    PropertyChanges {
                        target: rectangle
                        color: palette.highlight
                    }

                    PropertyChanges {
                        target: text
                        color: palette.highlightedText
                    }

                }
            ]

            Snippet {
                id: text

                color: palette.text
                itemModel: model
                width: parent.width
            }

            MouseArea {
                id: mouseArea

                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    list.currentIndex = index;
                    item.entryActivated(index, id);
                }
            }

        }

    }

}

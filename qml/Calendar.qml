import Qt.labs.calendar 1.0
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

GridLayout {
    property date selectedDate
    property date minimumDate
    property date maximumDate
    property int visibleMonth
    property int visibleYear

    function inRange(date) {
        return (isNaN(minimumDate) || date >= minimumDate) && (isNaN(maximumDate) || date <= maximumDate);
    }

    columns: 2
    onSelectedDateChanged: {
        visibleYear = selectedDate.getFullYear();
        visibleMonth = selectedDate.getMonth();
    }

    Button {
        id: left

        text: "<"
        Layout.fillWidth: true
        Layout.preferredWidth: Layout.minimumWidth
        onClicked: {
            if (visibleMonth === 0) {
                visibleMonth = 11;
                visibleYear -= 1;
            } else {
                visibleMonth -= 1;
            }
        }
        enabled: {
            const date = (visibleMonth === 0) ? new Date(visibleYear - 1, 11, 31) : new Date(visibleYear, visibleMonth - 1, 31);
            return inRange(date);
        }
    }

    RowLayout {
        Layout.fillWidth: true

        Label {
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: locale.standaloneMonthName(visibleMonth) + ' ' + visibleYear
        }

        Button {
            Layout.preferredWidth: left.width
            text: ">"
            onClicked: {
                if (visibleMonth === 11) {
                    visibleMonth = 0;
                    visibleYear += 1;
                } else {
                    visibleMonth += 1;
                }
            }
            enabled: {
                const date = (visibleMonth === 11) ? new Date(visibleYear + 1) : new Date(visibleYear, visibleMonth + 1, 0);
                return inRange(date);
            }
        }

    }

    Text {
    }

    DayOfWeekRow {
        Layout.fillWidth: true

        delegate: Label {
            text: model.shortName
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

    }

    WeekNumberColumn {
        month: grid.month
        year: grid.year
        Layout.fillHeight: true

        delegate: Label {
            text: model.weekNumber
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

    }

    MonthGrid {
        id: grid

        month: visibleMonth
        year: visibleYear
        Layout.fillHeight: true
        Layout.fillWidth: true
        onClicked: {
            if (inRange(date))
                selectedDate = date;

        }

        delegate: Rectangle {
            property bool sD: model.day === selectedDate.getDate() && model.month === selectedDate.getMonth() && model.year === selectedDate.getFullYear()
            property bool clickable: inRange(model.date)

            opacity: model.month === visibleMonth && model.year === visibleYear ? 1 : 0
            color: sD ? palette.highlight : (clickable ? (mouse.hovered ? palette.highlight : palette.button) : "transparent")

            Label {
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.fill: parent
                text: model.day
            }

            HoverHandler {
                id: mouse

                cursorShape: clickable ? Qt.PointingHandCursor : 0
            }

        }

    }

}

import "DateFunctions.js" as DateFunctions
import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    id: rectangle

    property var model
    property int columnStart
    property int columnDuration: 24 * 3600

    signal entryActivated(string id)

    border.width: 1
    border.color: "lightgrey"
    color: "transparent"

    Repeater {
        delegate: diaryEntry
        model: parent.model

        Component {
            id: diaryEntry

            DayDelegate {
                pixelsPerHour: d.pixelsPerHour
                columnWidth: rectangle.width
                itemModel: model
                columnStart: rectangle.columnStart
                columnDuration: rectangle.columnDuration
                onDoubleClicked: entryActivated(id)
            }

        }

    }

}

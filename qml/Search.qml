import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ColumnLayout {
    id: columnLayout

    signal entryActivated(string id)

    RowLayout {
        Layout.fillWidth: true

        TextField {
            Layout.fillWidth: true
            text: diaryModel.search.query
            placeholderText: qsTr('search')
            onTextChanged: diaryModel.search.query = text
        }

        Label {
            text: diaryModel.search.hitsCount
        }

    }

    DiaryEntryList {
        id: list

        Layout.fillWidth: true
        Layout.fillHeight: true
        model: diaryModel.search.hits
        onEntryActivated: columnLayout.entryActivated(id)
    }

}

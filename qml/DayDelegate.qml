import "DateFunctions.js" as DateFunctions
import QtQuick 2.15

Item {
    id: item

    property var itemModel
    property int pixelsPerHour
    property int columnWidth
    property int columnStart
    property int columnDuration
    property int minDuration: 3600

    signal doubleClicked()

    function calculateY() {
        console.assert(d.start > 0);
        let daySeconds = Math.max(0, d.start - columnStart);
        daySeconds = Math.min(daySeconds, columnDuration - minDuration);
        return daySeconds * d.pixelsPerSecond;
    }

    function calculateHeight() {
        console.assert(d.start > 0);
        console.assert(d.end > 0);
        let startSeconds = d.start;
        let endSeconds = d.end;
        startSeconds = Math.max(0, startSeconds - columnStart);
        endSeconds = Math.min(columnDuration, endSeconds - columnStart);
        const duration = Math.max(endSeconds - startSeconds, minDuration);
        return duration / pixelsPerHour;
    }

    function setStartAndEnd() {
        diaryModel.setStartAndEnd(itemModel.id, d.start, d.end);
        y = Qt.binding(function() {
            return calculateY();
        });
        height = Qt.binding(function() {
            return calculateHeight();
        });
        d.start = Qt.binding(function() {
            return itemModel.start;
        });
        d.end = Qt.binding(function() {
            return itemModel.end;
        });
    }

    x: d.x
    y: calculateY()
    width: d.articleWidth
    height: calculateHeight()

    QtObject {
        id: d

        property real pixelsPerSecond: pixelsPerHour / 3600
        property bool showTime: mouseArea.containsMouse || mouseArea.pressed || topMouse.containsMouse || topMouse.pressed || bottomMouse.containsMouse || bottomMouse.pressed
        property real articleWidth: columnWidth / itemModel.renderColumns
        property real x: itemModel.renderColumn * articleWidth
        property real start: itemModel.start
        property real end: itemModel.end
    }

    Rectangle {
        id: rect

        color: "lightgreen"
        border.width: 1
        border.color: palette.text
        height: item.height
        width: item.width
    }

    Snippet {
        id: main

        itemModel: parent.itemModel
        anchors.fill: parent
        showTime: false
        clip: true
    }

    MouseArea {
        id: mouseArea

        anchors.fill: parent
        hoverEnabled: true
        onDoubleClicked: item.doubleClicked()
        cursorShape: drag.active ? Qt.SizeAllCursor : Qt.ArrowCursor
        onPositionChanged: {
            if (drag.active) {
                const dy = Math.min(rect.y, item.height);
                if (dy !== 0) {
                    item.y += dy;
                    d.start += dy / d.pixelsPerSecond;
                    d.end += dy / d.pixelsPerSecond;
                }
                rect.y = 0;
            }
        }
        onReleased: setStartAndEnd()

        drag {
            target: rect
            axis: Drag.YAxis
        }

    }

    Rectangle {
        id: topBar

        width: parent.width
        height: 5
        color: "green"

        MouseArea {
            id: topMouse

            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.SizeVerCursor
            onPositionChanged: {
                if (drag.active) {
                    const dy = Math.min(topBar.y, item.height);
                    if (dy !== 0) {
                        item.height -= dy;
                        item.y += dy;
                        d.start += dy / d.pixelsPerSecond;
                    }
                    topBar.y = 0;
                }
            }
            onReleased: setStartAndEnd()

            drag {
                target: parent
                axis: Drag.YAxis
            }

        }

    }

    Rectangle {
        id: bottomBar

        width: parent.width
        y: parent.height
        height: 5
        color: "green"

        MouseArea {
            id: bottomMouse

            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.SizeVerCursor
            onPositionChanged: {
                if (drag.active) {
                    const newHeight = Math.max(bottomBar.y, 0);
                    if (newHeight !== 0) {
                        item.height = newHeight;
                        d.end = d.start + newHeight / d.pixelsPerSecond;
                    }
                    bottomBar.y = Qt.binding(function() {
                        return rect.height;
                    });
                }
            }
            onReleased: setStartAndEnd()

            drag {
                target: parent
                axis: Drag.YAxis
            }

        }

    }

    Text {
        id: startText

        color: palette.text
        visible: d.showTime
        y: topBar.y - height
        text: DateFunctions.formatSecsHour(locale, new Date(d.start))
        horizontalAlignment: Text.AlignHCenter
        width: parent.width
    }

    Text {
        id: endText

        color: palette.text
        visible: d.showTime
        y: topBar.y + item.height
        text: DateFunctions.formatSecsHour(locale, new Date(d.end))
        horizontalAlignment: Text.AlignHCenter
        width: parent.width
    }

}

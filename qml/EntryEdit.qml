import "./entryeditors"
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    id: entryEdit

    property var model

    signal newEntry(string entryType)
    signal newEnd(int end)

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            id: row

            Layout.fillWidth: true

            Label {
                text: model.entryType
            }

            TimeRangeInput {
                start: model.start
                end: model.end
                onNewStart: model.start = start
                onNewEnd: model.end = end
            }

            Button {
                id: new_button

                text: qsTr("new")
                onClicked: entryEdit.newEntry(entryType.currentText)
            }

            Button {
                id: done_now_button

                text: qsTr("done now")
                onClicked: {
                    const t = Date.now() / 1000;
                    model.end = t - t % 60;
                }
            }

            ComboBox {
                id: entryType

                model: diaryModel.entryTypes().split(",")
            }

            Button {
                text: qsTr("continue")
                onClicked: diaryModel.continueFrom(model.id)
            }

        }

        Item {
            id: row2

            Layout.fillWidth: true
            Layout.fillHeight: true

            Article {
                height: row2.height
                width: p.width
                visible: model.entryType === "Article"
                enabled: visible
            }

            Todo {
                height: row2.height
                width: p.width
                visible: model.entryType === "Todo"
                enabled: visible
            }

            ScrollView {
                id: body

                x: p.width
                height: row2.height
                width: p.width
                clip: true
                contentWidth: html_text.paintedWidth
                contentHeight: html_text.paintedHeight

                TextEdit {
                    id: html_text

                    color: palette.text
                    visible: true
                    width: p.width - p.scrollbar_width
                    text: model.rendered
                    textFormat: Text.RichText
                    wrapMode: TextEdit.WordWrap
                    readOnly: true
                    selectByMouse: true
                    padding: p.padding
                    onLinkActivated: Qt.openUrlExternally(link)
                }

            }

            QtObject {
                id: p

                readonly property real padding: 5
                readonly property real scrollbar_width: 18
                readonly property real width: row2.width / 2
            }

        }

    }

}

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    id: item

    property var model

    function setCurrentEntry(id) {
        diaryModel.setEditedEntry(id);
        list.setCurrentEntry(id);
    }

    function onEditedChanged() {
        const entries = diaryModel.entries;
        const entry = diaryModel.entry;
        if (!entries.selected)
            entries.selected = entry.id;

        diaryModel.setEditedEntry(entry.id);
    }

    width: parent.width
    Component.onCompleted: {
        const entries = diaryModel.entries;
        const entry = diaryModel.entry;
        entry.titleChanged.connect(onEditedChanged);
        entry.messageChanged.connect(onEditedChanged);
        if (entries.rowCount())
            setCurrentEntry(entries.id(0));

    }

    DiaryEntryList {
        id: list

        width: 200
        model: item.model
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        onEntryActivated: {
            diaryModel.setEditedEntry(id);
            diaryModel.save();
        }
    }

    EntryEdit {
        id: entryEdit

        model: diaryModel.entry
        Layout.fillWidth: true
        anchors.left: list.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        onNewEntry: {
            diaryModel.newEntry(entryType);
        }
    }

}

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import RustCode 1.0

ColumnLayout {
    TextField {
        id: title

        Layout.fillWidth: true
        text: model.title
        placeholderText: "title"
        onTextChanged: model.title = text
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        TextArea {
            id: area

            font: fixedFont
            text: model.message
            wrapMode: TextEdit.Wrap
            selectByMouse: true
            onTextChanged: model.message = text
            Keys.onSpacePressed: {
                if (event.modifiers === Qt.ControlModifier) {
                    const pos = area.cursorPosition;
                    const before = getText(Math.max(0, pos - 100), pos);
                    const after = getText(pos, Math.min(area.length, pos + 100));
                    console.log("Pressed ctrl-return", event);
                    console.log(before);
                    console.log(after);
                    const rect = area.positionToRectangle(pos);
                    autoComplete.x = rect.x;
                    autoComplete.y = rect.y + area.font.pixelSize;
                    //                autoComplete.focus = true;
                    autoComplete.visible = true;
                } else {
                    event.accepted = false;
                }
            }
            Keys.onEscapePressed: {
                autoComplete.visible = false;
            }

            ComboBox {
                id: autoComplete

                model: ["Banana", "Apple", "Coconut"]
                visible: false
            }

        }

    }

    EmailSyntaxHighlighter {
        document: area.textDocument
        dark: palette.base.hslLightness < 0.5
    }

}

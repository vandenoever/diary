import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ColumnLayout {
    RowLayout {
        Layout.fillWidth: true

        CheckBox {
            text: "done"
            checked: model.done || false
            onCheckedChanged: model.done = checked
        }

        Label {
            text: "geschatte duur"
        }

        SpinBox {
            from: 0
            to: 1e+09
            textFromValue: (value, locale) => {
                return value + " seconden";
            }
            value: model.estimatedDuration || 0
            onValueChanged: model.estimatedDuration = value
        }

    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        TextArea {
            text: model.message
            selectByMouse: true
            onTextChanged: model.message = text
        }

    }

}

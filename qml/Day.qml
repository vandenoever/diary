import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ColumnLayout {
    id: columnLayout

    signal newDay(int day)
    signal entryActivated(string id)

    QtObject {
        id: d

        property int pixelsPerHour: 60
        property int height: pixelsPerHour * 24
    }

    DaySelector {
        id: daySelector

        day: diaryModel.dayDay
        onNewDay: columnLayout.newDay(day)
    }

    TextMetrics {
        id: labelMetrics

        text: " 00:00 "
    }

    Flickable {
        id: flickable

        clip: true
        Layout.fillWidth: true
        Layout.fillHeight: true
        contentWidth: width
        contentHeight: d.height
        contentY: contentHeight - height

        Repeater {
            model: 24

            delegate: Item {
                y: index * d.pixelsPerHour

                Label {
                    width: labelMetrics.width
                    text: ((index > 9) ? "" : "0") + index + ":00"
                }

                Rectangle {
                    width: flickable.width - labelMetrics.width
                    height: d.pixelsPerHour
                    x: labelMetrics.width
                    color: (index % 2) ? "transparent" : palette.base
                }

            }

        }

        DayColumn {
            x: labelMetrics.width
            width: flickable.width
            height: flickable.contentHeight
            model: diaryModel.day
            columnStart: daySelector.day
            onEntryActivated: columnLayout.entryActivated(id)
        }

    }

}

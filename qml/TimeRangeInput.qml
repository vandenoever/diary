import "DateFunctions.js" as DateFunctions
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Button {
    id: button

    property int start
    property int end

    signal newStart(int start)
    signal newEnd(int end)

    function updateControls() {
        const s = new Date(start * 1000);
        let e = new Date(end * 1000);
        if (e < s)
            e = s;

        startHours.to = 23;
        startMinutes.to = 59;
        endHours.from = 0;
        endMinutes.from = 0;
        text = DateFunctions.formatDateRange(locale, s, e);
        startLabel.text = DateFunctions.formatDate(locale, s);
        endLabel.text = DateFunctions.formatDate(locale, e);
        startHours.valueChanged.disconnect(setStart);
        startMinutes.valueChanged.disconnect(setStart);
        endHours.valueChanged.disconnect(setEnd);
        endMinutes.valueChanged.disconnect(setEnd);
        startHours.value = s.getHours();
        startMinutes.value = s.getMinutes();
        endHours.value = e.getHours();
        endMinutes.value = e.getMinutes();
        const sameDay = s.toDateString() === e.toDateString();
        const sameHour = sameDay && s.getHours() === e.getHours();
        startHours.to = sameDay ? endHours.value : 23;
        startMinutes.to = sameHour ? endMinutes.value : 59;
        endHours.from = sameDay ? startHours.value : 0;
        endMinutes.from = sameHour ? startMinutes.value : 0;
        startHours.valueChanged.connect(setStart);
        startMinutes.valueChanged.connect(setStart);
        endHours.valueChanged.connect(setEnd);
        endMinutes.valueChanged.connect(setEnd);
        startCalendar.selectedDateChanged.disconnect(setStart);
        startCalendar.maximumDate = roundToEndOfDay(e);
        startCalendar.selectedDate = s;
        startCalendar.selectedDateChanged.connect(setStart);
        endCalendar.selectedDateChanged.disconnect(setEnd);
        endCalendar.minimumDate = roundToStartOfDay(s);
        endCalendar.selectedDate = e;
        endCalendar.selectedDateChanged.connect(setEnd);
    }

    // Round the date to the start of the day.
    // This is needed because the date is rounded to the nearest day when
    // used with minimumDate
    function roundToStartOfDay(date) {
        const d = new Date(date.getTime());
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        return d;
    }

    // Round the date to the end of the day.
    // This is needed because the date is rounded to the nearest day when
    // used with maximumDate
    function roundToEndOfDay(date) {
        const d = new Date(date.getTime());
        d.setHours(23);
        d.setMinutes(59);
        d.setSeconds(59);
        return d;
    }

    function dateFromControls(calendar, hours, minutes) {
        const d = new Date(0);
        const cd = calendar.selectedDate;
        d.setFullYear(cd.getFullYear());
        d.setMonth(cd.getMonth());
        d.setDate(cd.getDate());
        d.setHours(hours.value);
        d.setMinutes(minutes.value);
        const t = d.getTime() / 1000;
        return t - t % 60;
    }

    function setStart() {
        newStart(dateFromControls(startCalendar, startHours, startMinutes));
    }

    function setEnd() {
        newEnd(dateFromControls(endCalendar, endHours, endMinutes));
    }

    function renderValue(value, locale) {
        const prefix = (value >= 10) ? '' : '0';
        return prefix + Number(value).toString();
    }

    onClicked: {
        dialog.visible = true;
    }
    onStartChanged: updateControls()
    onEndChanged: updateControls()

    Dialog {
        id: dialog

        standardButtons: Dialog.Close

        RowLayout {
            ColumnLayout {
                Label {
                    id: startLabel
                }

                RowLayout {
                    Label {
                        text: qsTr("start")
                    }

                    SpinBox {
                        id: startHours

                        from: 0
                        to: 23
                        textFromValue: renderValue
                    }

                    SpinBox {
                        id: startMinutes

                        from: 0
                        to: 59
                        textFromValue: renderValue
                    }

                }

                Calendar {
                    id: startCalendar
                }

            }

            ColumnLayout {
                Label {
                    id: endLabel
                }

                RowLayout {
                    Label {
                        text: qsTr("end")
                    }

                    SpinBox {
                        id: endHours

                        from: 0
                        to: 23
                        textFromValue: renderValue
                    }

                    SpinBox {
                        id: endMinutes

                        from: 0
                        to: 59
                        textFromValue: renderValue
                    }

                }

                Calendar {
                    id: endCalendar
                }

            }

        }

    }

}

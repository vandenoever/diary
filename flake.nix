{
  description = "Diary GUI";
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    let
      systems = with utils.lib.system; [
        aarch64-linux
        i686-linux
        x86_64-linux
      ];
    in utils.lib.eachSystem systems (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = naersk.lib."${system}";
        inputs = with pkgs; [
          pkg-config
          cmake
          libsForQt5.qt5.qtdeclarative
          libsForQt5.qt5.wrapQtAppsHook
          libsForQt5.syntax-highlighting
          libsForQt5.qqc2-breeze-style
          libsForQt5.kirigami2
        ];
      in rec {
        # `nix build`
        packages.diary = naersk-lib.buildPackage {
          pname = "diary";
          root = ./.;
          nativeBuildInputs = inputs;
        };
        packages.default = packages.diary;

        # `nix run`
        apps.diary = utils.lib.mkApp { drv = packages.diary; };
        apps."${system}".default = apps.diary;

        # `nix develop`
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs;
            [ rustc cargo clippy rustfmt rust-analyzer ] ++ inputs;
        };
      });
}

